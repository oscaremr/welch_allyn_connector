/**
 * Copyright (c) 2013. Enterprise EMR Solutions Inc. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Enterprise EMR Solutions Inc. BC
 * Mukaila Olundegun
 * October 2013
 */
package com.plynet.wa.welch_allyn_connector.ws;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.List;

import javax.jws.WebService;

import org.oscarehr.oscar.ws.manager.DemographicManager;
import org.oscarehr.oscar.ws.manager.MeasurementManager;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.ws.DemographicTransfer;
import org.oscarehr.ws.MeasurementTransfer;
import org.oscarehr.ws.NotAuthorisedException_Exception;

import com.plynet.wa.welch_allyn_connector.util.Constants;
import com.plynet.wa.welch_allyn_connector.util.UtilS;
import com.plynet.wa.welch_allyn_connector.ws.ServiceMain;

@WebService(targetNamespace = "http://ws.welch_allyn_connector.oscar.oscarehr.org/", portName = "WaEmrServicePort", serviceName = "WaEmrServiceService")
public class WaEmrService
{
	
	public static ServiceMain credentials;
	/**
	 * Used authenticate user
	 * username and password must be oscar server login and password respectively
	 * @param username 
	 * @param password
	 * @param myId
	 * @return uuid - SecurityToken
	 */
	
	
	public String userLogin(String username, String password, String myId)
	{

		String uuid = null;
		ServiceMain.setEnvironmentParameter();
		
		if ((uuid = wsLogin(username, password))!= null){
				return uuid;
		}
		return Constants.msgLoginFailed;

	}

	public static String wsLogin(String username, String password)
	{
		ServiceMain.setEnvironmentParameter();
		try {
			// this is to allow self signed certificates
			MiscUtils.setJvmDefaultSSLSocketFactoryAllowAllCertificates();
	    	credentials = ServiceMain.loginAndStoreInSession(ServiceMain.WsdlUrl, username, password, null);
   		} catch (KeyManagementException e) {
	    	UtilS.getLogger().info("" + e);
			return null;
		} catch (NoSuchAlgorithmException e) {
	    	UtilS.getLogger().info("" + e);
			return null;
		}
		catch (NotAuthorisedException_Exception e) {
	    	UtilS.getLogger().info("" + e);
			return null;
		}
		return credentials.getLoggedInPersonSecurityToken();
	}
	
	
	/**
	 * use to logout user
     * @param uuid
     * @param username
     * @return uuid
	 */
	public String userLogout(String uuid, String username)
	{

		try
		{
			if (!ServiceMain.checkUserUuid(uuid, username).equalsIgnoreCase(uuid)) 
				return Constants.msgUuidError;
		}
		catch (Exception e)
		{
	    	UtilS.getLogger().info("" + e);
			return Constants.msgUuidError;
		}
		return uuid;
	}
	
	/**
	 * This will search for patient and return the result back to client
	 * @param uuid - SecurityToken 
	 * @param username
	 * @param toSearch
	 * @return demo - search result
	 */
	public String searchPatient(String uuid, String username, String toSearch)
	{
		String hin=null;
		String firstname=null;
		String lastname=null;

		if(!credentials.getLoggedInPersonSecurityToken().equalsIgnoreCase(uuid)){
			return Constants.msgUuidError;
		}

		toSearch = toSearch.trim();
		if (toSearch.matches("^[0-9]+$") && toSearch.length() > 0)
		{
			hin = toSearch;
		
		}
		else
		{
			String[] names = toSearch.split(",");
			if (names != null && names.length > 0)
			{
				if (names.length > 1)
				{
					lastname = names[0];
					firstname = names[1];
					if (lastname != null && lastname.length() == 0 ) lastname=null;
					if (firstname != null && firstname.length() == 0 ) firstname=null;
				}
				else
				{
					lastname = names[0];
				}
			}
			else return Constants.msgSearchSqlError;
		}
		// get a list of all demographics in oscar and print to screen			
		String demo="";
		List<DemographicTransfer> allDemographics = DemographicManager.searchDemographicsByAttributes(credentials, hin, firstname,lastname, null, null, null, null, null, null, null, 0, 99);
		if(!allDemographics.isEmpty()){
			for (DemographicTransfer d : allDemographics){
				if (demo.length() > 0)  demo = demo + ":";
				demo = demo + d.getHin() + "," + d.getLastName().replace(",", " ") + "," + d.getFirstName().replace(",", " ") + ","+ d.getDateOfBirth().replace(",", " ") + ","
					+ d.getSex() + "," + d.getDemographicNo() + "," + d.getProviderNo();
			}	
		}
		else{
			demo=null;
		}
		return demo;
	}

	/**
	 * WT,99,in kg:HT,100,in cm:BMI,30,'':02SA,90,%:BP,120/97,mmHg:MAP,132,'':HR,70,bpm:PAIN,20,'':BTMP,37,deg C"       
	 * weight=xx,unit=xx:;-> WT,99,in kg; format:WT,value,unit:
	 * height=xx,unit=xx:;HT (in cm)
	 * BMI=xx,unit=xx:  BMI
	 * temperature; BTMP=xx,unit=deg C; 
	 * BP=SYSTOLIC/DIASTOLIC -> BP,xx/yy,mmHg
	 * Respiration HR=xx,unit=bpm ;
	 * 02SA=xx,unit=%; Oxygen sat.
	 * MAP (BP mean value); MAP=xx,unit=''; Mean Arterial Pressure
	 * PAIN=xx,unit='';
	 * date=yyyy-mm-dd hh:mm:00:
	 * measurement value not greater than 0 will not be uploaded.
	 * save mesurement data to oscar
	 * 
	 * @param uuid
	 * @param username
	 * @param demographic_no
	 * @param mDate
	 * @param measurement
	 * @return
	 */
	public String saveMeasurement(String uuid, String username, String demographic_no, String mDate, String measurement)
	{

		if(!credentials.getLoggedInPersonSecurityToken().equalsIgnoreCase(uuid)){
			return Constants.msgUuidError;
		}

		if (demographic_no == null || demographic_no.length() == 0 || measurement == null || measurement.length() == 0) return null;

		String[] str = measurement.split(":");
		if(str != null && str.length == 0) return null;
		String val0, val1, val2;
		Integer retSave;
		String descp;
		String comment = "Auto-Upload;";
		String providerNo;
        String [] xmlParam = new String[str.length] ;
        String [] xmlValue = new String[str.length] ;
        String [] xmlUnit  = new String[str.length] ;
        String [] xmlDescription = new String[str.length] ;
		DemographicTransfer demographic = DemographicManager.getDemographic(credentials, UtilS.getIntegerValue(demographic_no));
		if(demographic == null) return null;
		providerNo = demographic.getDemographicNo().toString();
		MeasurementTransfer measurementTransfer = new MeasurementTransfer();

		try
		{
			float valFloat = 0.00F;
			for (int i = 0; i < str.length; i++)
			{
				String[] div = str[i].split(",");
				val0 = val1 = val2 = "";
				if (div.length > 0) val0 = div[0]; //measurement type
				if (div.length > 1) val1 = div[1]; //value
				if (div.length > 2) val2 = div[2]; //unit
				valFloat = 0.0F;
				try
				{
					String[] bpType = val1.split("/");
					if (bpType.length > 1)
					{
						Float valFloat1 = 0.0F;
						valFloat = Float.parseFloat(bpType[0]);
						valFloat1 = Float.parseFloat(bpType[1]);
						if (valFloat == 0.0F || valFloat1 == 0.0F) valFloat = 0.0F;
					}
					else
					{
						if (val1 != null && val1.length() > 0) valFloat = Float.parseFloat(val1);
					}
				}
				catch (Exception e)
				{
			    	UtilS.getLogger().info("" + e);
					valFloat = 0.00F;
				}

				if (val0 != null && valFloat > 0.0F && (ServiceMain.getMeasurementType(val0)) != null){
					val2 = ServiceMain.getMeasurementUnit(val0);
					descp = ServiceMain.getMeasurementDescription(val0);
					val0 = ServiceMain.getMeasurementType(val0);
					measurementTransfer.setAppointmentNo(0);
					measurementTransfer.setComments(comment);
					measurementTransfer.setCreateDate(Calendar.getInstance());
					measurementTransfer.setDemographicId(UtilS.getIntegerValue(demographic_no));
					measurementTransfer.setMeasuringInstruction(val2);
					measurementTransfer.setDataField(val1);
					measurementTransfer.setProviderNo(providerNo);
					measurementTransfer.setType(val0);
					measurementTransfer.setDateObserved(Calendar.getInstance());
					xmlParam[i]= val0; //measurement type
					xmlValue[i]= val1; //value
					xmlUnit[i] = val2; //unit
					xmlDescription[i] = descp;
					if(!ServiceMain.testMode.equalsIgnoreCase("yes")){
						retSave  = MeasurementManager.addMeasurement(credentials, measurementTransfer);
						if (retSave == null || retSave <= 0) 
							return Constants.msgNotSave;
					}
				}
			}
			if (ServiceMain.CreateXml != null && ServiceMain.CreateXml.equalsIgnoreCase("yes")) 
				ServiceMain.saveParameterChangesAsXML(demographic_no, xmlParam, xmlValue, xmlUnit, xmlDescription, mDate, uuid);
		}
		catch (Exception e)
		{
	    	UtilS.getLogger().info("" + e);
			return Constants.msgNotSave;
		}

		return Constants.msgSaved;

	}
	
}
