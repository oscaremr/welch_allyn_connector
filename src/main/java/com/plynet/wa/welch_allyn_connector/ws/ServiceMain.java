
/**
 * Copyright (c) 2013. Enterprise EMR Solutions Inc. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Enterprise EMR Solutions Inc. BC
 * Mukaila Olundegun
 * October 2013
 */
package com.plynet.wa.welch_allyn_connector.ws;

import java.io.File;
//import java.io.FileOutputStream;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.GregorianCalendar;
//import java.util.Properties;




import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

//import org.apache.log4j.Logger;



import org.oscarehr.oscar.ws.manager.LoginManager;
import org.oscarehr.oscar.ws.utils.OscarServerCredentials;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.ws.LoginResultTransfer;
import org.oscarehr.ws.NotAuthorisedException_Exception;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.plynet.wa.welch_allyn_connector.util.Constants;
import com.plynet.wa.welch_allyn_connector.util.PropertiesS;
import com.plynet.wa.welch_allyn_connector.util.UtilS;

public class ServiceMain implements OscarServerCredentials
{

	private static final String SESSION_KEY = ServiceMain.class.getName() + ".SESSION_KEY";
	//
	public static String weight="";
	public static String height="";
	public static String respiration="";
	public static String pain="";
	public static String oxygenSaturation="";
	public static String bloodPressure="";
	public static String bp_MAP="";
	public static String temperature="";
	public static String bodyMassIndex="";
	//
	public static String weightUnit="";
	public static String heightUnit="";
	public static String respirationUnit="";
	public static String painUnit="";
	public static String oxygenSaturationUnit="";
	public static String bloodPressureUnit="";
	public static String bp_MAPUnit="";
	public static String temperatureUnit="";
	public static String bodyMassIndexUnit="";	
  	
	public static String WsdlUrl         = "";
    public static String Homedir    	 = "";
    public static String WsdlUserName    = "";
    public static String WsdlPassword    = "";
    public static String WsdlKeyPhrase   = "";
    public static String WsdlCertificate = "";
    public static String WsdlKeyFile     = "";
    public static String TempDir         = "";
    public static String CreateXml    	 = "";
    public static String XmlDir    		 = "";
    public static String DownloadDir     = "";
    public static String LogDir          = "";
    public static String testMode        = "";

	static ServiceMain credentials;
	private String oscarWsUrl;
	private Integer oscarSecurityId;
	private String oscarSecurityTokenKey;
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{
		String username = "oscardoc";
		String password = "mac2002";

		WaEmrService waEmrService = new WaEmrService();

		setEnvironmentParameter();
		String token = waEmrService.userLogin(username, password, "201391903528424");
		System.out.println(waEmrService.searchPatient(token, "oscardoc","dem"));
		String m = "WT,68.4,in kg:HT,180.3,in cm:BMI,21.0,:BTMP,36.4,deg C:BP,120/90,mmHg:MAP,134,:HR,78,in bpm:02SA,98,%:PAIN,5,";
		System.out.println(waEmrService.saveMeasurement(token, "oscardoc", "2", "2013-09-18 02_28_00", m));
	}

	/**
	 * 
	 * @param oscarWsUrl
	 * @param loginResultTransfer
	 */
	public ServiceMain(String oscarWsUrl, LoginResultTransfer loginResultTransfer)
	{
		this(oscarWsUrl, loginResultTransfer.getSecurityId(), loginResultTransfer.getSecurityTokenKey());
	}

	/**
	 * 
	 * @param oscarWsUrl
	 * @param oscarSecurityId
	 * @param oscarSecurityTokenKey
	 */
	public ServiceMain(String oscarWsUrl, Integer oscarSecurityId, String oscarSecurityTokenKey)
	{
		this.oscarWsUrl = oscarWsUrl;
		this.oscarSecurityId = oscarSecurityId;
		this.oscarSecurityTokenKey = oscarSecurityTokenKey;
	}
	

	@Override
	public String getServerBaseUrl()
	{
		return(oscarWsUrl);
	}

	@Override
	public Integer getLoggedInSecurityId()
	{
		return(oscarSecurityId);
	}

	@Override
	public String getLoggedInPersonSecurityToken()
	{
		return(oscarSecurityTokenKey);
	}


	/**
	 * This method will login and return credentials of the logged in person.
	 * @param session can be null 
	 */
	//static HttpSession session=null;
	public static ServiceMain loginAndStoreInSession(String oscarWsUrl, String oscarUserName, String oscarPassword, HttpSession session) throws NotAuthorisedException_Exception
	{
		LoginResultTransfer loginResultTransfer = LoginManager.login(oscarWsUrl, oscarUserName, oscarPassword);

		ServiceMain credentials = new ServiceMain(oscarWsUrl, loginResultTransfer);
		
		if (session != null) session.setAttribute(SESSION_KEY, credentials);

		return(credentials);
	}
    
	/**
	 * 
	 * @param session
	 * @return
	 */
	public static ServiceMain getOscarServerCredentialsFromSession(HttpSession session)
	{
		return (ServiceMain) (session.getAttribute(SESSION_KEY));
	}
	
	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public static boolean wsLogin(String username, String password)
	{
		
		try {
			// this is to allow self signed certificates
			MiscUtils.setJvmDefaultSSLSocketFactoryAllowAllCertificates();
	    	credentials = ServiceMain.loginAndStoreInSession(WsdlUrl, username, password, null);
   		} catch (KeyManagementException e) {
	    	UtilS.getLogger().info("" + e);
			return false;
		} catch (NoSuchAlgorithmException e) {
	    	UtilS.getLogger().info("" + e);
			return false;
		}
		catch (NotAuthorisedException_Exception e) {
	    	UtilS.getLogger().info("" + e);
			return false;
		}
		
		return true;
	}
	
	/**
	 * Set default parameter value
	 */
	public static void setEnvironmentParameter()
	{
		String dosPrompt = UtilS.getDosPrompt();
		PropertiesS pro  = new PropertiesS();
		Homedir     = dosPrompt + pro.getHomedir(Constants.defaultHome);   
		Homedir     = Homedir.endsWith("/") ? Homedir : Homedir + "/";   
		LogDir      = Homedir + Constants.defaultLogDir;  

		WsdlUrl     = pro.getWsdlUrl();
		DownloadDir = Homedir + Constants.defaultDownloadDir;  
		TempDir     = Homedir + Constants.defaultTempDir;
		CreateXml   = pro.getCreateXml(Constants.defaultCreateXml);
		XmlDir      = pro.getXmlDir(TempDir);
		XmlDir      = XmlDir.endsWith("/") ? XmlDir : XmlDir + "/";	
		testMode    = pro.getTestMode(Constants.defaultTestMode);
		//
		weight = pro.getWeight(Constants.defaultWeight);  
		height = pro.getHeight(Constants.defaultHeight);  
		respiration = pro.getRespiration(Constants.defaultRespiration);  
		pain = pro.getPain(Constants.defaultPain);  
		oxygenSaturation = pro.getOxygenSaturation(Constants.defaultOxygenSaturation);  
		bloodPressure = pro.getBloodPressure(Constants.defaultBloodPressure);  
		bp_MAP = pro.getBp_MAP(Constants.defaultBP_MAP);  
		temperature = pro.getTemperature(Constants.defaultTemperature);  
		bodyMassIndex = pro.getBodyMassIndex(Constants.defaultBodyMassIndex);  	
		//
		weightUnit = pro.getWeightUnit(Constants.defaultWeightUnit);  
		heightUnit = pro.getHeightUnit(Constants.defaultHeightUnit);  
		respirationUnit = pro.getRespirationUnit(Constants.defaultRespirationUnit);  
		painUnit = pro.getPainUnit(Constants.defaultPainUnit);  
		oxygenSaturationUnit = pro.getOxygenSaturationUnit(Constants.defaultOxygenSaturationUnit);  
		bloodPressureUnit = pro.getBloodPressureUnit(Constants.defaultBloodPressureUnit);  
		bp_MAPUnit        = pro.getBp_MAPUnit(Constants.defaultBP_MAPUnit);  
		temperatureUnit   = pro.getTemperatureUnit(Constants.defaultTemperatureUnit);  
		bodyMassIndexUnit = pro.getBodyMassIndexUnit(Constants.defaultBodyMassIndexUnit);  	
	}

	/**
	 * create uuid to use for client login
	 * @return
	 */
	public static String generateUUID()
	{
		Calendar calendar = GregorianCalendar.getInstance();
		String tnow = calendar.get(Calendar.YEAR) + "" + (calendar.get(Calendar.MONTH) + 1) + calendar.get(Calendar.DATE) + calendar.get(Calendar.HOUR_OF_DAY) + calendar.get(Calendar.MINUTE) + calendar.get(Calendar.SECOND) + calendar.get(Calendar.MILLISECOND);

		return tnow;
	}


    /**
     * check the validity of user's uuid
     * @param uuid
     * @param username
     * @return
     */
	public static String checkUserUuid(String uuid, String username)
	{
		String localUuid = credentials.getLoggedInPersonSecurityToken();
		if(uuid != null && uuid.equalsIgnoreCase(localUuid))
			return uuid;
		else
			return Constants.msgUuidLoadError;
	}

	/**
	 * 
	 * @param mType
	 * @return
	 */
	public static String getMeasurementType(String mType){

		   if( mType.equalsIgnoreCase(Constants.defaultWeight) ) return weight; 
		   else if( mType.equalsIgnoreCase(Constants.defaultHeight)) return height;
		   else if( mType.equalsIgnoreCase(Constants.defaultRespiration)) return respiration;
		   else if( mType.equalsIgnoreCase(Constants.defaultBloodPressure)) return bloodPressure;
		   else if( mType.equalsIgnoreCase(Constants.defaultBodyMassIndex)) return bodyMassIndex;
		   else if( mType.equalsIgnoreCase(Constants.defaultOxygenSaturation)) return oxygenSaturation;
		   else if( mType.equalsIgnoreCase(Constants.defaultTemperature)) return temperature;
		   else if( mType.equalsIgnoreCase(Constants.defaultBP_MAP)) return bp_MAP;
		   else if( mType.equalsIgnoreCase(Constants.defaultPain)) return pain;
		   else return null;
	   }

	   /**
	    * 
	    * @param mUnit
	    * @return
	    */
	   public static String getMeasurementUnit(String mUnit){

		   if( mUnit.equalsIgnoreCase(Constants.defaultWeight) ) return weightUnit; 
		   else if( mUnit.equalsIgnoreCase(Constants.defaultHeight)) return heightUnit;
		   else if( mUnit.equalsIgnoreCase(Constants.defaultRespiration)) return respirationUnit;
		   else if( mUnit.equalsIgnoreCase(Constants.defaultBloodPressure)) return bloodPressureUnit;
		   else if( mUnit.equalsIgnoreCase(Constants.defaultBodyMassIndex)) return bodyMassIndexUnit;
		   else if( mUnit.equalsIgnoreCase(Constants.defaultOxygenSaturation)) return oxygenSaturationUnit;
		   else if( mUnit.equalsIgnoreCase(Constants.defaultTemperature)) return temperatureUnit;
		   else if( mUnit.equalsIgnoreCase(Constants.defaultBP_MAP)) return bp_MAPUnit;
		   else if( mUnit.equalsIgnoreCase(Constants.defaultPain)) return painUnit;
		   else return null;
	   }
	   /**
	    * 
	    * @param mDescription
	    * @return
	    */
	   public static String getMeasurementDescription(String mDescription){

		   if( mDescription.equalsIgnoreCase(Constants.defaultWeight) ) return Constants.descriptionWeight; 
		   else if( mDescription.equalsIgnoreCase(Constants.defaultHeight)) return Constants.descriptionHeight;
		   else if( mDescription.equalsIgnoreCase(Constants.defaultRespiration)) return Constants.descriptionRespiration;
		   else if( mDescription.equalsIgnoreCase(Constants.defaultBloodPressure)) return Constants.descriptionBloodPressure;
		   else if( mDescription.equalsIgnoreCase(Constants.defaultBodyMassIndex)) return Constants.descriptionBodyMassIndex;
		   else if( mDescription.equalsIgnoreCase(Constants.defaultOxygenSaturation)) return Constants.descriptionOxygenSaturation;
		   else if( mDescription.equalsIgnoreCase(Constants.defaultTemperature)) return Constants.descriptionTemperature;
		   else if( mDescription.equalsIgnoreCase(Constants.defaultBP_MAP)) return Constants.descriptionBP_MAP;
		   else if( mDescription.equalsIgnoreCase(Constants.defaultPain)) return Constants.descriptionPain;
		   else return null;
	   }

	/**
	 * save the raw data to xml file for other emr use
	 * @param demographic_no
	 * @param xmlParam
	 * @param xmlValue
	 * @param xmlUnit
	 * @param mDate
	 * @param uuid
	 * @return
	 */
	public static boolean saveParameterChangesAsXML(String demographic_no, String [] xmlParam, String [] xmlValue, String [] xmlUnit, String [] xmlDescription,String  mDate, String uuid) {
		boolean ret = true;
		String setupFilePath = XmlDir + "/" + demographic_no + "_" + generateUUID() + ".xml";
		mDate = mDate.replaceAll("_", ":");
		if (mDate==null || mDate.length()==0) mDate=" ";
		
		try {
			Document doc;
	        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
	    	DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
	    	
	    	// root elements
	    	doc = docBuilder.newDocument();
	    	doc.setXmlVersion("1.0");
	    	doc.setXmlStandalone(true);
	    	
 	    	Element rootElement = doc.createElement("Measurement");
	    	doc.appendChild(rootElement);
	    	//
	    	Attr  attr= doc.createAttribute("source");
    		attr.setValue(Constants.emrSource);
    		rootElement.setAttributeNode(attr);
	    	//
	    	attr= doc.createAttribute("collectDate");
    		attr.setValue(mDate);
    		rootElement.setAttributeNode(attr);
    		//
	    	attr= doc.createAttribute("provider");
    		attr.setValue(Constants.emrProvider);
    		rootElement.setAttributeNode(attr);

    		
    		for(int i=0; i<xmlParam.length;i++){
    			if (xmlParam[i]==null || xmlParam[i].length()==0) continue;
    			if (xmlValue[i]==null || xmlValue[i].length()==0) xmlValue[i]=" ";;
    			if (xmlUnit[i]==null || xmlUnit[i].length()==0) xmlUnit[i]=" ";
    			if (xmlDescription[i]==null || xmlDescription[i].length()==0) xmlDescription[i]=" "; 
 
    			// metric elements
        		Element measData = doc.createElement("mData");
        		rootElement.appendChild(measData);
    	    	Attr  attrData= doc.createAttribute("description");
        		attrData.setValue(xmlDescription[i]);
        		measData.setAttributeNode(attrData);    			
	    		//
	    		Element data1 = doc.createElement("mParam");
	    		data1.appendChild(doc.createTextNode(xmlParam[i]));
	    		measData.appendChild(data1);
	    		//
	    		Element data2 = doc.createElement("mValue");
	    		data2.appendChild(doc.createTextNode(xmlValue[i]));
	    		measData.appendChild(data2);
	    		//
	    		Element data3 = doc.createElement("mUnit");
	    		data3.appendChild(doc.createTextNode(xmlUnit[i]));
	    		measData.appendChild(data3);
    		}
    		// write the content into xml file Welchallyn Measurement Data by Enterprise EMR Solutions Inc
    		TransformerFactory transformerFactory = TransformerFactory.newInstance();
    		Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
    		DOMSource source = new DOMSource(doc);
    		StreamResult result = new StreamResult(new File(setupFilePath));

    		//Output to console for testing
    		//StreamResult result = new StreamResult(System.out);
    		transformer.transform(source, result);

		 }
	     catch (Exception e ) {
	    	UtilS.getLogger().info("" + e);
		    ret = false;
		 }
		
		return ret;
		
	}
	
}
