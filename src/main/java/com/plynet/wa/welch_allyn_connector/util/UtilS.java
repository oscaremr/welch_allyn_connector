/**
 * Copyright (c) 2013. Enterprise EMR Solutions Inc. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Enterprise EMR Solutions Inc. BC
 * Mukaila Olundegun
 * October 2013
 */
package com.plynet.wa.welch_allyn_connector.util;

import java.io.File;
import java.io.UnsupportedEncodingException;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;



public class UtilS {

	public static final String ENCODING = "UTF-8";
	private static final Base64 base64 = new Base64();
	
	/**
	 * This method will return a logger instance which has 
	 * the name based on the class that's calling this method.
	 */
	public static Logger getLogger(){
		StackTraceElement[] ste = Thread.currentThread().getStackTrace();
		String caller = ste[2].getClassName();
		return(Logger.getLogger(caller));
	}
	
	/**
	 * Used to set log messages.
	 * In case of debug mode is able.
	 * @author Particular
	 * @param logger
	 * @param message
	 */
	public static void setLog(Logger logger, String message){
		//Enable debug 0, 1,2,4,5,6; higher number more information; 
		// 0 : no debug
		int debugMode = PropertiesS.getInstance().getDebug();
		switch(debugMode){
			case 1: logger.info(logger.getClass()+" - MSG: "+message);break;
			case 2: logger.info(logger.getClass()+" - MSG: "+message);
					System.out.println(logger.getName()+" - MSG: "+message);
		}		
	}
	
	/**
	 * Used to set log error
	 * In case of debug mode is able.
	 * @author Particular
	 * @param logger
	 * @param message
	 */
	public static void setLogError(Logger logger, Exception ex){
		//Enable debug 0, 1,2,4,5,6; higher number more information; 
		// 0 : no debug
		int debugMode = PropertiesS.getInstance().getDebug();
		switch(debugMode){
			case 1: logger.error(logger.getClass()+" - ERROR: "+ex.getMessage());break;
			case 2: logger.error(logger.getClass()+" - ERROR: "+ex.getMessage());
					System.out.println(logger.getName()+" - ERROR: "+ex.getMessage());
		}		
	}
	
	/**
	 * Used to set log error
	 * In case of debug mode is able.
	 * @author Particular
	 * @param logger
	 * @param message
	 */
	public static void setLogError(Logger logger, String msg, Exception ex){
		//Enable debug 0, 1,2,4,5,6; higher number more information; 
		// 0 : no debug
		int debugMode = PropertiesS.getInstance().getDebug();
		switch(debugMode){
			case 1: logger.error(logger.getClass()+" - ERROR: "+msg+" >>> "+ex.getMessage());break;
			case 2: logger.error(logger.getClass()+" - ERROR: "+msg+" >>> "+ex.getMessage());
					System.out.println(logger.getName()+" - ERROR: "+msg+" >>> "+ex.getMessage());
		}		
	}
	
	
	
	/**
	 * 
	 * @param s
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String encodeToBase64String(String s) throws UnsupportedEncodingException {
		return (new String(base64.encode(s.getBytes(ENCODING)), ENCODING));
	}

	/**
	 * 
	 * @param s
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String decodeBase64StoString(String s) throws UnsupportedEncodingException {
		return (new String(base64.decode(s.getBytes(ENCODING)), ENCODING));
	}
	
	/**
	 * remove white space
	 * @param s
	 * @return
	 */
	public static String getString(String s){
		return s!=null ? s.trim() : "";
	}
	
	/**
	 * check for operating system platform
	 * @return
	 */
	public static String  getDosPrompt(){
		String Dosprompt="";
	  		
		String ss= System.getProperty("user.dir");
		String osName = System.getProperty("os.name").toLowerCase();
        if (osName.startsWith("windows")) {
        	Dosprompt = ss.substring(0, 2);
 		}	
      
    	return Dosprompt;
    }

	/**
	 * convert string to integer
	 * @param str
	 * @return
	 */
	public static Integer  getIntegerValue(String str)
	{
		Integer ret=0;
		try{
			ret = Integer.parseInt(str);
		}
		catch (Exception e){
			ret = 0;
		}
		return ret;
	}
	
	/**
	 * 
	 * @return ret
	 */
	public static String getWorkPropeties(){
		
		String ret=null;
		String dosprompt = getDosPrompt();
	    String properties=null; 
	    File propFile  = null;

	    java.net.URL url = Thread.currentThread().getContextClassLoader().getResource( Constants.defaultPropertiesName );
	    if( url != null ){
	    	properties = url.getFile();
	    	if(properties.startsWith("/") && !dosprompt.isEmpty()){
	    		properties = properties.replaceFirst("/","");
	    	}
	    	propFile  = new File(properties);
	    }

	    if (propFile != null && propFile.isFile())  {
			ret = properties;
		}
		else {
			properties = dosprompt  + Constants.defaultHome + Constants.defaultPropertiesName;  
			propFile  = new File(properties);
			if (propFile != null && propFile.isFile())  {
				ret = properties;
			} 
		}
		return ret;
	}

}
