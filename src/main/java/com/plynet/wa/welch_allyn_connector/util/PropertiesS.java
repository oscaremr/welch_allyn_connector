/**
 * Copyright (c) 2013. Enterprise EMR Solutions Inc. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Enterprise EMR Solutions Inc. BC
 * Mukaila Olundegun
 * October 2013
 */
package com.plynet.wa.welch_allyn_connector.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
 
/**
 * This class holds system properties.  
 * Every time the properties file changes, tomcat must be restarted.
 */
public class PropertiesS extends Properties {
	private static final long serialVersionUID = -5965807410049845132L;
	private static PropertiesS waProperties = new PropertiesS();

	/**
	 * @return  the instance of WA.Properties
	 */
	public static PropertiesS getInstance() {
		return waProperties;
	}
	
	public PropertiesS(){
		String overrideProperties =  UtilS.getWorkPropeties(); 
		try {
						
			if (overrideProperties != null && !overrideProperties.isEmpty()) {
				readFromFile(overrideProperties);
			}else{
				System.out.println("Can not find properties file "+ Constants.defaultPropertiesName);
			}
		} 
		catch (Exception e) {
			System.out.println("Can not read properties file "+ overrideProperties +"\n" + e.getMessage());
		}
	}
	
	/**
	 * 
	 * @param url
	 * @throws IOException
	 */
	public void readFromFile(String url) throws IOException {
		InputStream is = getClass().getResourceAsStream(url);
		try {
			if (is == null){
				is = new FileInputStream(url);
			}
			load(is);
		} finally {
			is.close();
		}
	}
	

	/**
	 * 
	 * @return
	 */
	public String getPDFDocumentDir(){
		return getProperty("PDFDocumentDir");
	}


	/**
	 * 
	 * @return
	 */
	public int getDebug(){
		String prop = getProperty("Debug");
		int ret=0;
		try{
			ret=Integer.parseInt(prop);
		}
		catch(NumberFormatException e){
			UtilS.getLogger().info("Debug Parameter: " + e);
		}

		return ret;	
	}
	/**
	 * 
	 * @param defaultTestMode
	 * @return
	 */
	public String getTestMode(String defaultTestMode){
		return getProperty("TestMode",defaultTestMode);	
	}
	/**
	 * 
	 * @return
	 */
	public String getMysqlHost(){
		return getProperty("MysqlHost");	
	}
	
	/**
	 * 
	 * @return
	 */
	public String getMySqlDriver(){
		return getProperty("MySqlDriver");	
	}
	/**
	 * 
	 * @return
	 */
	public String getMysqlUri(){
		return getProperty("MysqlUri");	
	}
	
	/**
	 * 
	 * @return
	 */
	public String getMysqlPort(){
		return getProperty("MysqlPort");	
	}
	
	/**
	 * 
	 * @return
	 */
	public String getMysqlDatabase(){
		return getProperty("MysqlDatabase");	
	}
	
	/**
	 * 
	 * @return
	 */
	public String getMysqlUserName(){
		return getProperty("MysqlUserName");	
	}
	
	/**
	 * 
	 * @return
	 */
	public String getMysqlPassword(){
		return getProperty("MysqlPassword");	
	}
	
	/**
	 * 
	 * @return
	 */
	public String getMysqlPoolSize(){
		return getProperty("MysqlPoolSize");	
	}
	
	/**
	 * 
	 * @return
	 */
	public String getMysqlDialect(){
		return getProperty("MysqlDialect");	
	}

	/**
	 * 
	 * @return
	 */
	public String getMysqlShowSql(){
		return getProperty("MysqlShowSql");	
	}
	
	/**
	 * 
	 * @return
	 */
	public String getMysqlFormatSql(){
		return getProperty("MysqlFormatSql");	
	}	
    /**
     * 
     * @return
     */
	public String getHomedir(String defaultHome){
		return getProperty("Homedir",defaultHome);	
	}

	/**
	 * 
	 * @return
	 */
	public String getCompleteDir() {
		return getProperty("CompleteDir");	
	}
	
	/**
	 * 
	 * @return
	 */
	public String getWsdlUserName() {
		return getProperty("WsdlUserName");	
	}

	/**
	 * 
	 * @return
	 */
	public String getWsdlPassword() {
		return getProperty("WsdlPassword");	
	}
	
	/**
	 * 
	 * @return
	 */
	public String getXmlDir(String defaultXmlDir) {
		return getProperty("XmlDir",defaultXmlDir);	
	}
	
	/**
	 * 
	 * @return
	 */
	public String getCreateXml(String defaultCreateXml) {
		return getProperty("CreateXml",defaultCreateXml);	
	}
	
	/**
	 * 
	 * @param defaultMeasurementType
	 * @return
	 */
	public String getWeight(String defaultMeasurementType) {
		return getProperty("Weight",defaultMeasurementType);	
	}
	
	/**
	 * 
	 * @param defaultMeasurementType
	 * @return
	 */
	public String getHeight(String defaultMeasurementType) {
		return getProperty("Height",defaultMeasurementType);	
	}
	
	/**
	 * 
	 * @param defaultMeasurementType
	 * @return
	 */
	public String getRespiration(String defaultMeasurementType) {
		return getProperty("Respiration",defaultMeasurementType);	
	}
	
	/**
	 * 
	 * @param defaultMeasurementType
	 * @return
	 */
	public String getPain(String defaultMeasurementType) {
		return getProperty("Pain",defaultMeasurementType);	
	}
	
	/**
	 * 
	 * @param defaultMeasurementType
	 * @return
	 */
	public String getOxygenSaturation(String defaultMeasurementType) {
		return getProperty("OxygenSaturation",defaultMeasurementType);	
	}
	
	/**
	 * 
	 * @param defaultMeasurementType
	 * @return
	 */
	public String getBloodPressure(String defaultMeasurementType) {
		return getProperty("BloodPressure",defaultMeasurementType);	
	}
	
	/**
	 * 
	 * @param defaultMeasurementType
	 * @return
	 */
	public String getBp_MAP(String defaultMeasurementType) {
		return getProperty("BP_MAP",defaultMeasurementType);	
	}
	
	/**
	 * 
	 * @param defaultMeasurementType
	 * @return
	 */
	public String getTemperature(String defaultMeasurementType) {
		return getProperty("Temperature",defaultMeasurementType);	
	}
	
	/**
	 * 
	 * @param defaultMeasurementType
	 * @return
	 */
	public String getBodyMassIndex(String defaultMeasurementType) {
		return getProperty("BodyMassIndex",defaultMeasurementType);	
	}	

	/**
	 * 
	 * @return
	 */
	public String getWsdlUrl() {
		return getProperty("WsdlUrl");	
	}	
	/**
	 * 
	 * @param defaultMeasurementUnit
	 * @return
	 */
	public String getWeightUnit(String defaultMeasurementUnit) {
		return getProperty("WeightUnit",defaultMeasurementUnit);	
	}
	/**
	 * 
	 * @param defaultMeasurementUnit
	 * @return
	 */
	public String getHeightUnit(String defaultMeasurementUnit) {
		return getProperty("HeightUnit",defaultMeasurementUnit);	
	}
	/**
	 * 
	 * @param defaultMeasurementUnit
	 * @return
	 */
	public String getRespirationUnit(String defaultMeasurementUnit) {
		return getProperty("RespirationUnit",defaultMeasurementUnit);	
	}
	/**
	 * 
	 * @param defaultMeasurementUnit
	 * @return
	 */
	public String getPainUnit(String defaultMeasurementUnit) {
		return getProperty("PainUnit",defaultMeasurementUnit);	
	}
	/**
	 * 
	 * @param defaultMeasurementUnit
	 * @return
	 */
	public String getOxygenSaturationUnit(String defaultMeasurementUnit) {
		return getProperty("OxygenSaturationUnit",defaultMeasurementUnit);	
	}
	/**
	 * 
	 * @param defaultMeasurementUnit
	 * @return
	 */
	public String getBloodPressureUnit(String defaultMeasurementUnit) {
		return getProperty("BloodPressureUnit",defaultMeasurementUnit);	
	}
	/**
	 * 
	 * @param defaultMeasurementUnit
	 * @return
	 */
	public String getBp_MAPUnit(String defaultMeasurementUnit) {
		return getProperty("BP_MAPUnit",defaultMeasurementUnit);	
	}
	/**
	 * 
	 * @param defaultMeasurementUnit
	 * @return
	 */
	public String getTemperatureUnit(String defaultMeasurementUnit) {
		return getProperty("TemperatureUnit",defaultMeasurementUnit);	
	}
	/**
	 * 
	 * @param defaultMeasurementUnit
	 * @return
	 */
	public String getBodyMassIndexUnit(String defaultMeasurementUnit) {
		return getProperty("BodyMassIndexUnit",defaultMeasurementUnit);	
	}
}
