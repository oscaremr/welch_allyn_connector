/**
 * Copyright (c) 2013. Enterprise EMR Solutions Inc. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Enterprise EMR Solutions Inc. BC
 * Mukaila Olundegun
 * October 2013
 */

package com.plynet.wa.welch_allyn_connector.util;

public class Constants {
	public static String msgNotAllow = "LoginNotAllow";        //Not allow to read file without login first
	public static String msgLoginOK = "LoginOK";               //Login successful
	public static String msgLoginFailed = "LoginFailed";       //username and/or password is not correct
	public static String msgNotSave  = "MeasurementNotSave";   //Error has occurred during sql query or can not login to database
	public static String msgSaved    = "MeasurementSaved";     // measurement data saved successfully
	public static String msgSearchError = "SearchError";       //error occured during sql query or illegal parameter from client
	public static String msgSearchSqlError = "SearchSqlError"; //error occurred during sql query or can not login to database
	public static String msgOK = "OK";                         //Command OK.
	public static String msgError = "Error";                   //process error
	public static String msgUuidFileError = "UuidFileError";   //can not open specified uuid file or file not available;
	public static String msgUuidLoadError = "UuidLoadError";    //can not read content of specified uuid file;
	public static String msgUuidError = "UuidError";            //can not read content of specified uuid file; This can be as a result of msgUuidFileError or msgUuidLoadError 
	//
	public static final String defaultWeight="WT";
	public static final String defaultHeight="HT";
	public static final String defaultRespiration="HR";
	public static final String defaultPain="PAIN";
	public static final String defaultOxygenSaturation="02SA";
	public static final String defaultBloodPressure="BP";
	public static final String defaultBP_MAP="MAP";
	public static final String defaultTemperature="BTMP";
	public static final String defaultBodyMassIndex="BMI";
    //parameter description 
	public static final String descriptionWeight="Weight";
	public static final String descriptionHeight="Height";
	public static final String descriptionRespiration="Respiration";
	public static final String descriptionPain="Pain";
	public static final String descriptionOxygenSaturation="Oxygen Saturation";
	public static final String descriptionBloodPressure="Blood Pressure";
	public static final String descriptionBP_MAP="Mean Arterial Pressure";
	public static final String descriptionTemperature="Body Temperature";
	public static final String descriptionBodyMassIndex="Body Mass Index";
	//	
	public static final String defaultWeightUnit="in kg";
	public static final String defaultHeightUnit="in cm";
	public static final String defaultRespirationUnit="in bpm";
	public static final String defaultPainUnit="";
	public static final String defaultOxygenSaturationUnit="%";
	public static final String defaultBloodPressureUnit="mmHg";
	public static final String defaultBP_MAPUnit="";
	public static final String defaultTemperatureUnit="deg C";
	public static final String defaultBodyMassIndexUnit="";
	//
    public static String defaultHome      = "/usr/local/lib/welchallyn/";
    public static String defaultTempDir   = "tmp/";
    public static String defaultCreateXml = "no";
    public static String defaultDownloadDir      = "download/";
    public static String defaultLogDir           = "logs/";
    public static String defaultCertificateDir   = "certificate/";
    public static String defaultWsdlPropertyFile = "config.properties"; //should be at the project root directory
    public static int    Debug                   = 1;
    public static String defaultTestMode         = "no";
    public static String emrProvider             = "Enterprise EMR Solutions Inc.";
    public static String emrSource               = "Welch Allyn Vitals Monitoring Device";
    //
    public static String defaultPropertiesName   = "welchallyn.properties";

}
